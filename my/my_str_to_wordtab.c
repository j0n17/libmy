/*
** my_str_to_wordtab.c for my_str_to_wordtab in /home/maire_j/rendu/PSU_2018_minishell1
**
** Made by maire_j
** Login   <maire_j@epitech.net>
**
** Started on Fri Nov  8 09:35:13 2013 Maire Jonathan
** Last update Fri Nov  8 10:29:57 2013 Maire Jonathan
*/

#include <stdlib.h>

int	my_countword(char *str)
{
  int	i;
  int	word;

  i = 0;
  word = 2;
  while (str[i] && str[i] != '\n')
    {
      if (str[i] == ' ')
	word = word + 1;
      i = i + 1;
    }
  return (word);
}

int	my_countchar(char *str)
{
  int	c;

  c = 0;
  while (str[c] != ' ' && str[c] && str[c] != '\n')
    c = c + 1;
  c = c + 1;
  return (c);
}

char    **my_str_to_wordtab(char *str)
{
  char	 **tab;
  int    i;
  int    b;
  int    a;

  b = 0;
  i = 0;
  a = 0;
  if (str[i] == ' ')
    while (str[i] == ' ')
      i = i + 1;
  tab = malloc(sizeof(*tab) * (my_countword(str)));
  while (str[i] != '\n' && str[i])
    {
      if (str[i] == ' ' || str[i] == '\n')
	{
	  while (str[i] == ' ')
	    i = i + 1;
	  a = a + 1;
	  b = 0;
	}
      tab[a] = malloc(sizeof(**tab) * (my_countchar(str + i)));
      while (str[i] != ' '  && str[i] != '\n' && str[i])
	tab[a][b++] = str[i++];
      tab[a][b] = '\0';
    }
  tab[a + 1] = 0;
  return (tab);
}
