/*
** my_revstr.c for my_revstr.c in /home/maire_j/rendu/Piscine-C-Jour_06/ex_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 09:31:03 2013 Maire Jonathan
** Last update Thu Oct 10 20:04:23 2013 Maire Jonathan
*/

#include "my.h"

char	*my_revstr(char *str)
{
  char	c_tmp;
  int	i;
  int	i1;

  i = 0;
  i1 = my_strlen(str) - 1;
  while (i < i1)
    {
      c_tmp = str[i];
      str[i] = str[i1];
      str[i1] = c_tmp;
      i = i + 1;
      i1 = i1 - 1;
    }
  return (str);
}

