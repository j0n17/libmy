/*
** my_putchar.c for my_putchar in /home/maire_j/rendu/Piscine-C-lib
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 06:50:08 2013 Maire Jonathan
** Last update Tue Oct  8 07:05:36 2013 Maire Jonathan
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}
