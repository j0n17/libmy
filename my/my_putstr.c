/*
** my_putstr.c for my_putstr in /home/maire_j/rendu/Piscine-C-lib
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 07:03:57 2013 Maire Jonathan
** Last update Tue Oct  8 09:19:16 2013 Maire Jonathan
*/

#include "my.h"

int	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i = i + 1;
    }
  return (0);
}
