/*
** my_strcat.c for lib in /home/maire_j/Desktop/Programmation_C/fakeC
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 10 20:23:13 2013 Maire Jonathan
** Last update Sun Nov  3 23:01:35 2013 Maire Jonathan
*/

#include <stdlib.h>
#include "my.h"

char	*my_strcat(char *dest, char *src)
{
  int   lenght_dest;
  int   i;

  lenght_dest = my_strlen(dest);
  i = 0;
  while (src[i] != '\0')
    {
      dest[lenght_dest + i] = src[i];
      i = i + 1;
    }
  dest[lenght_dest + i] = '\0';
  return (dest);
}
