/*
** my_strlowcase.c for lib in /home/maire_j/rendu/Piscine-C-lib/my
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 11 14:51:43 2013 Maire Jonathan
** Last update Fri Oct 11 14:59:29 2013 Maire Jonathan
*/

char	*my_strlowcase(char *str)
{
  int	i;
  
  i = 0;
  while (str[i])
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	{
	  str[i] = str[i] + 32;
	}
      i = i + 1;
    }
  return (str);
}
