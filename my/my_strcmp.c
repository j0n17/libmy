/*
** my_strcmp.c for lib in /home/maire_j/Desktop/Programmation_C/fakeC
**
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
**
** Started on  Thu Oct 10 20:19:40 2013 Maire Jonathan
** Last update Sun Nov 10 01:19:25 2013 Maire Jonathan
*/

#include "my.h"

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  if (my_strlen(s1) < my_strlen(s2))
    return (-1);
  else if (my_strlen(s1) > my_strlen(s2))
    return (1);
  else
    {
      while (s1[i] && s2[i])
	{
	  if (s1[i] > s2[i])
	    return (1);
	  else if (s1[i] < s2[i])
	    return (-1);
	  i = i + 1;
	}
      return (0); 
    }
}
