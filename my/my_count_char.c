/*
** my_count_char.c for lib in /home/maire_j/Desktop/Programmation_C/fakeC
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 11 10:30:18 2013 Maire Jonathan
** Last update Fri Oct 11 10:30:48 2013 Maire Jonathan
*/

int	my_count_char(char *str, char c)
{
  int   i;
  int   counter;

  i = 0;
  counter = 0;
  while (str[i])
    {
      if (str[i] == c)
        {
          counter = counter + 1;
        }
      i = i + 1;
    }
  return (counter);
}
