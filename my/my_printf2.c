/*
** func.c for func in /home/maire_j/rendu/myprintf-2017ed_maire_j/srcs
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Nov 16 15:54:56 2013 
** Last update Sun Nov 17 19:47:39 2013 
*/

#include <stdlib.h>
#include "my.h"

int	decimal_octal(int n, int b)
{
  int	res;
  int	mult;

  res = 0;
  mult = 1;
  while (n != 0)
    {
      res = res + (n % b) * mult;
      n = n / b;
      mult = mult * 10;
    }
  return (res);
}

void	my_putstrf(char *str, int *nb, int option)
{
  int	i;

  i = 0;
  while (str[i])
    {
      *nb = *nb + 1;
      if (option == 0)
	my_putchar(str[i]);
      if (option == 1)
	{
	  if (str[i] < 32 || str[i] > 127)
	    {
	      my_putchar('\\');
	      my_putnbr(decimal_octal((int)str[i], 8));
	    }
	  else
	    my_putchar(str[i]);
	}
      i = i + 1;
    }
}

int	chk_flag(char c)
{
  char	*flag;
  int	i;

  i = 0;
  if ((flag = malloc(18 * sizeof(char))) == 0)
      return (-1);
  flag = my_strdup("dsSonc%");
  while (flag[i])
    {
      if (flag[i] == c)
	return (i);
      i = i + 1;
    }
  return (-1);
}

int	chk_format(char *str)
{
  int	i;
  int	nb;

  i = 0;
  nb = 0;
  while (str[i])
    {
      if (str[i] == '%' && (str[i + 1] != '%' && str[i + 1]))
	{
	  if (chk_flag(str[i + 1]) == -1)
	    {
	      my_putstr("Error, one of the flag is invalid !\n");
	      return (0);
	    }
	  nb = nb + 1;
	}
      i = i + 1;
    }
  return (nb);
}
