/*
** my_put_nbr.c for my_put_nbr.c in /home/maire_j/Desktop/Programmation_C/Test
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 10 16:00:37 2013 Maire Jonathan
** Last update Mon Oct 21 23:22:17 2013 Maire Jonathan
*/

#include "my.h"

int	my_nbr_len(int nb)
{
  int	i;

  i = 1;
  while (nb > 9)
    {
      nb = nb / 10;
      i = i + 1;
    }
  return (i);
}

int	powpow(int pow)
{
  int	i;
  int	nb;

  i = 1;
  nb = 1 ;
  while (i <= pow)
    {
      nb = nb * 10;
      i = i + 1;
    }
  return (nb);
}

void	write_nbr(int nb)
{
  int   size;
  int   mod;
  int   divider;
  int   result;

  if (nb < 0)
    {
      my_putchar('-');
      nb = -nb;
    }
  result = nb;
  size = my_nbr_len(nb) - 1;
  mod = powpow(size) * 10;
  divider = powpow(size);
  while (size >= 0)
    {
      result = nb % mod;
      result = result / divider;
      my_putchar(result + 48);
      mod = mod / 10;
      divider = divider / 10;
      size = size - 1;
    }
}

int	my_putnbr(int nb)
{
  if (nb == -2147483648)
    {
      my_putstr("101");
    }
  else
    {
      write_nbr(nb);
    }
  return (0);
}
