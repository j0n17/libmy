/*
** main.c for main.c in /home/maire_j/rendu/myprintf-2017ed_maire_j/srcs
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Nov 16 15:06:35 2013 
** Last update Wed Nov 20 15:21:51 2013 
*/

#include <stdarg.h>
#include <stdlib.h>
#include "my.h"

void	choose_fnc(char *format, int *i, va_list ap, int *nb)
{
  *i = *i + 1;
  if (format[*i] == 's')
    my_putstrf((char *)va_arg(ap, char *), nb, 0);
  if (format[*i] == 'S')
    my_putstrf((char *)va_arg(ap, char *), nb, 1);
  if (format[*i] == 'd')
    my_putnbr((int)va_arg(ap, int));
  if (format[*i] == 'c')
    my_putchar((int)va_arg(ap, int));
  if (format[*i] == '%')
    my_putchar('%');
  if (format[*i] == 'o')
    {
      my_putchar('0');
      my_putnbr((int)va_arg(ap, int));
    }
  *i = *i + 1;
}

int	my_printf(char *format, ...)
{
  va_list ap;
  int	nb;
  int	i;

  i = 0;
  nb = 0;
  if (chk_format(format) == 0)
    return (0);
  va_start(ap, format);
  while (format[i])
    {
      if (format[i] == '%' && format[i + 1] == 'n')
	my_putnbr(nb);
      if (format[i] == '%' && format[i + 1] != '\0')
	choose_fnc(format, &i, ap, &nb);
      my_putchar(format[i]);
      i = i + 1;
    }
  va_end(ap);
  return (0);
}
