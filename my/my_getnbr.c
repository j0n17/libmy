/*
** my_getnbr.c for my_getnbr.c in /home/maire_j/Desktop/Programmation_C/fakeC
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 10 20:12:50 2013 Maire Jonathan
** Last update Tue Oct 22 09:17:40 2013 Maire Jonathan
*/

int	count_char(char c, char *str)
{
  int	i;
  int	result;

  i = 0;
  result = 0;
  while ((str[i] == c && str[i]) && ((str[i] < 48 || str[i] > 57) || str[i] == 43))
    {
      result = result + 1;
      i = i + 1;
    }
  return (result);
}

int	transform_nbr(char *str)
{
  int   result;
  int   i;

  i = 0;
  result = 0;
  while ((str[i] && str[i] >= 48 && str[i] <= 57) || str[i] == 43 || str[i] == 45)
    {
      if (str[i] >= '0' && str[i] <= '9')
        {
          result = result * 10;
          result = result + (str[i] - 48);
        }
      i = i + 1;
    }
  return (result);
}

int	my_getnbr(char *str)
{
  int	result;

  if (count_char('-', str) % 2 == 0)
    {
      result = transform_nbr(str);
    }
  else
    {
      result = transform_nbr(str);
      result = -result;
    }
  return (result);
}
