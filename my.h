/*
** my.h for my.h in /home/maire_j/rendu/Piscine-C-include/
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 10 09:01:52 2013 Maire Jonathan
** Last update Thu Oct 31 16:00:51 2013 Maire Jonathan
*/

#ifndef MY_H_
# define MY_H_

void	my_putchar(char);
int	my_isneg(int);
int	my_putnbr(int);
int	my_swap(int *, int *);
int	my_putstr(char *);
int	my_strlen(char *);
int	my_getnbr(char *);
void	my_sort_int_tab(int *, int);
int	my_power_rec(int, int);
int	my_square_root(int);
int	my_is_prime(int);
int	my_find_prime_sup(int);
char	*my_strcpy(char *, char *);
char	*my_strncpy(char *, char *, int);
char	*my_revstr(char *);
char	*my_strstr(char *, char *);
int	my_strcmp(char *, char *);
int	my_strncmp(char *, char *, int);
char	*my_strupcase(char *);
char	*my_strlowcase(char *);
char	*my_strcapitalize(char *);
int	my_str_isalpha(char *);
int	my_str_isnum(char *);
int	my_str_islower(char *);
int	my_str_isupper(char *);
int	my_str_isprintable(char *);
int	my_showstr(char *);
int	my_showmem(char *, int);
char	*my_strcat(char *, char *);
char	*my_strncat(char *, char *, int);
int	my_strlcat(char *, char *, int);
char	**my_str_to_wordtab(char *);
int	my_show_wordtab(char **);
char	*my_strdup(char *);
int	my_printf(char *, ...);


/**************my_printf**************/

int	decimal_octal(int, int);
void	my_putstrf(char *, int *, int);
int	chk_flag(char);
int	chk_format(char *);

/*************************************/

#endif /* MY_H_ */
